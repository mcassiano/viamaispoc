package br.com.concrete.viamaispoc.feature.account

import android.arch.lifecycle.Lifecycle.Event.ON_CREATE
import android.arch.lifecycle.Lifecycle.Event.ON_DESTROY
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v4.app.FragmentManager
import br.com.concrete.viamaispoc.R
import br.com.concrete.viamaispoc.navigation.NavigationDispatcher
import br.com.concrete.viamaispoc.navigation.NavigationEvent
import br.com.concrete.viamaispoc.util.runTransaction
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber


class ShowLoginNavigationEvent : NavigationEvent(true)
class ShowRegistrationNavigationEvent : NavigationEvent(false)

class AccountFlowNavigator(
        private val activity: AccountFlowActivity,
        private val fragmentManager: FragmentManager,
        private val dispatcher: NavigationDispatcher
) : LifecycleObserver {

    init {
        activity.lifecycle.addObserver(this)
    }

    private val disposables = CompositeDisposable()

    @OnLifecycleEvent(value = ON_CREATE)
    fun registerObserver() {
        Timber.i("${this.javaClass} is observing navigation events")
        disposables.addAll(
                dispatcher.events
                        .ofType(ShowLoginNavigationEvent::class.java)
                        .subscribe(this::showLogin),
                dispatcher.events
                        .ofType(ShowRegistrationNavigationEvent::class.java)
                        .subscribe(this::showRegistration)
        )
    }

    @OnLifecycleEvent(value = ON_DESTROY)
    fun unregisterObserver() {
        Timber.i("${this.javaClass} stopped observing navigation events")
        disposables.clear()
        activity.lifecycle.removeObserver(this)
    }

    private fun showLogin(event: ShowLoginNavigationEvent) {
        Timber.i("Received Navigation Event: $event")
        fragmentManager.runTransaction {
            replace(R.id.frame_layout, LoginFragment())
        }
    }

    private fun showRegistration(event: ShowRegistrationNavigationEvent) {
        Timber.i("Received Navigation Event: $event")
    }

}