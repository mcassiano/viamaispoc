package br.com.concrete.viamaispoc.injection

import android.app.Application
import br.com.concrete.viamaispoc.App
import br.com.concrete.viamaispoc.R
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class
])
interface AppComponent : AndroidInjector<App> {

    override fun inject(app: App)


    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun baseUrl(url: UrlConfig): Builder
    }
}

enum class Flavor {
    Default,
    Flavor1,
    Flavor2
}

data class UrlConfig(
        val baseUrl: String,
        val mediaUrl: String
)

data class AppConfig(
        var flavor: Flavor = Flavor.Default
) {
    val theme: Int
        get() = when (flavor) {
            Flavor.Flavor1 -> R.style.AppTheme_Flavor1
            Flavor.Flavor2 -> R.style.AppTheme_Flavor2
            else -> R.style.AppTheme
        }
}