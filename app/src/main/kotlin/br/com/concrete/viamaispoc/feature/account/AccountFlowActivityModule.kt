package br.com.concrete.viamaispoc.feature.account

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import br.com.concrete.viamaispoc.injection.ActivityScope
import br.com.concrete.viamaispoc.injection.ViewModelKey
import br.com.concrete.viamaispoc.navigation.NavigationDispatcher
import br.com.concrete.viamaispoc.util.ScopedViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class AccountFlowActivityModule {

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment

    @ActivityScope
    @Binds
    abstract fun bindViewModelFactory(factory: ScopedViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginFlowViewModel::class)
    abstract fun loginFlowViewModel(mainViewModel: LoginFlowViewModel): ViewModel

    @Module
    companion object {

        @ActivityScope
        @Provides
        @JvmStatic
        fun providesNavigator(
                activity: AccountFlowActivity,
                dispatcher: NavigationDispatcher
        ): AccountFlowNavigator {
            val fragmentManager = activity.supportFragmentManager
            return AccountFlowNavigator(activity, fragmentManager, dispatcher)
        }
    }

}