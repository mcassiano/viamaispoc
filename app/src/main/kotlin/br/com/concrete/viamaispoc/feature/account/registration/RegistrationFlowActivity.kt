package br.com.concrete.viamaispoc.feature.account.registration

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.concrete.viamaispoc.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*

class RegistrationFlowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        setToolbar()
    }

    private fun setToolbar() {
        toolbar.run {
            setSupportActionBar(this)
            supportActionBar?.run {
                setDisplayShowTitleEnabled(true)
                setDisplayHomeAsUpEnabled(true)
            }
            setNavigationOnClickListener { onBackPressed() }
        }
    }
}