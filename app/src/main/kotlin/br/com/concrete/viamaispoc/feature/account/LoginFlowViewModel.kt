package br.com.concrete.viamaispoc.feature.account

import android.arch.lifecycle.ViewModel
import br.com.concrete.viamaispoc.injection.AppConfig
import br.com.concrete.viamaispoc.injection.Flavor
import br.com.concrete.viamaispoc.navigation.NavigationDispatcher
import br.com.concrete.viamaispoc.navigation.NavigationEvent
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

class LoginFailedException : Exception()
class LoginEmptyException : Exception()

class LoginSuccessfulNavigationEvent : NavigationEvent()

sealed class LoginScreenState {
    object Idle : LoginScreenState()
    object Loading : LoginScreenState()
    data class Error(val error: Throwable) : LoginScreenState()
}

@Singleton
class LoginRepository @Inject constructor() {

    fun login(username: String, password: String): Completable = Completable.defer {
        if (username.isBlank() || password.isBlank()) {
            return@defer Completable.error(LoginEmptyException())
        } else if (validCredentials(username, password))
            Completable
                    .complete()
                    .delay(2, TimeUnit.SECONDS)
        else Completable.error(LoginFailedException())
    }

    private fun validCredentials(u: String, p: String) =
            (u == "flavor1" || u == "flavor2") && p == "12345"

}

class LoginFlowViewModel @Inject constructor(
        private val appConfig: AppConfig,
        private val dispatcher: NavigationDispatcher,
        private val loginRepository: LoginRepository
) : ViewModel() {

    private val _state = BehaviorSubject.create<LoginScreenState>()
    private val disposables = CompositeDisposable()

    val state: Observable<LoginScreenState>
        get() = _state

    init {
        dispatcher.postEvent(ShowLoginNavigationEvent())
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun loginWith(username: String, password: String) {

        if (username.isBlank() || password.isBlank()) {
            _state.onNext(LoginScreenState.Error(LoginFailedException()))
            return
        }

        disposables.add(loginRepository
                .login(username, password)
                .doOnSubscribe { _state.onNext(LoginScreenState.Loading) }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    _state.onNext(LoginScreenState.Idle)
                    appConfig.flavor = when (username) {
                        "flavor1" -> Flavor.Flavor1
                        "flavor2" -> Flavor.Flavor2
                        else -> Flavor.Default
                    }

                    dispatcher.postEvent(LoginSuccessfulNavigationEvent())
                }, {
                    _state.onNext(LoginScreenState.Error(it))
                    _state.onNext(LoginScreenState.Idle)
                }))
    }

}