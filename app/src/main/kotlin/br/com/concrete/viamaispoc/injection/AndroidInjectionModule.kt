package br.com.concrete.viamaispoc.injection

import br.com.concrete.viamaispoc.feature.account.AccountFlowActivity
import br.com.concrete.viamaispoc.feature.account.AccountFlowActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidInjectionModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [AccountFlowActivityModule::class])
    abstract fun mainActivity(): AccountFlowActivity

}