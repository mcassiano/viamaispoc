package br.com.concrete.viamaispoc.navigation

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

abstract class NavigationEvent(val popAll: Boolean = false)

@Singleton
class NavigationDispatcher @Inject constructor() {

    private val _events = BehaviorSubject.create<NavigationEvent>()

    val events: Observable<NavigationEvent>
        get() = _events

    fun postEvent(event: NavigationEvent) {
        Timber.i("${this.javaClass} is dispatching $event")
        _events.onNext(event)
    }

}