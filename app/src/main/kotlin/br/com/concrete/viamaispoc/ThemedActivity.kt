package br.com.concrete.viamaispoc

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.concrete.viamaispoc.injection.AppConfig
import javax.inject.Inject

abstract class ThemedActivity : AppCompatActivity() {

    @Inject
    lateinit var appConfig: AppConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appConfig.theme)
        super.onCreate(savedInstanceState)
    }
}