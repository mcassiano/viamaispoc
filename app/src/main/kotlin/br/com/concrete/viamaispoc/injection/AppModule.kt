package br.com.concrete.viamaispoc.injection

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [AndroidInjectionModule::class])
abstract class AppModule {


    @Singleton
    @Binds
    abstract fun bindContext(app: Application): Context

    @Module
    companion object {

        @JvmStatic
        @Singleton
        @Provides
        fun provideAppConfig() = AppConfig()
    }

}

