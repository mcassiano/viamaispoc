package br.com.concrete.viamaispoc.injection

import android.arch.lifecycle.ViewModel
import dagger.MapKey
import javax.inject.Scope
import kotlin.reflect.KClass


@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Retention(value = AnnotationRetention.RUNTIME)
@Scope
annotation class ActivityScope

@Retention(value = AnnotationRetention.RUNTIME)
@Scope
annotation class FragmentScope