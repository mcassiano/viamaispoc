package br.com.concrete.viamaispoc.feature.account

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import br.com.concrete.viamaispoc.R
import br.com.concrete.viamaispoc.navigation.NavigationDispatcher
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class AccountFlowActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigator: AccountFlowNavigator

    @Inject
    lateinit var navigationDispatcher: NavigationDispatcher

    lateinit var viewModel: LoginFlowViewModel

    override fun supportFragmentInjector() = fragmentInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders
                .of(this, modelFactory)[LoginFlowViewModel::class.java]
        setContentView(R.layout.activity_main)
        setToolbar()
    }

    private fun setToolbar() {
        toolbar.run {
            setSupportActionBar(this)
            supportActionBar?.run {
                setDisplayShowTitleEnabled(true)
                setDisplayHomeAsUpEnabled(true)
            }
            setNavigationOnClickListener { onBackPressed() }
        }
    }

}

