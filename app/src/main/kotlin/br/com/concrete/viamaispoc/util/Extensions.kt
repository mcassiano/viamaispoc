package br.com.concrete.viamaispoc.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.widget.Toast

fun FragmentManager.runTransaction(block: FragmentTransaction.() -> Unit) {
    with(beginTransaction()) {
        block()
        commit()
    }
}

fun FragmentManager.runTransactionNow(block: FragmentTransaction.() -> Unit) {
    with(beginTransaction()) {
        block()
        commitNow()
    }
}

fun Fragment.toastLong(text: String) {
    Toast.makeText(context,
            text,
            Toast.LENGTH_LONG
    ).show()
}