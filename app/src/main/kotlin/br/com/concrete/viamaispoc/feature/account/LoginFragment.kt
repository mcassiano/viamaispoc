package br.com.concrete.viamaispoc.feature.account

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.concrete.viamaispoc.R
import br.com.concrete.viamaispoc.util.toastLong
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_login.*
import timber.log.Timber
import javax.inject.Inject

class LoginFragment : Fragment() {

    private val disposables = CompositeDisposable()

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory

    lateinit var viewModel: LoginFlowViewModel

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders
                .of(activity as AppCompatActivity, modelFactory)[LoginFlowViewModel::class.java]

        login_button.setOnClickListener {
            val username = username_field.text.toString()
            val password = password_field.text.toString()
            viewModel.loginWith(username, password)
        }

        with(viewModel) {
            disposables.add(state.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this@LoginFragment::render))
        }
    }

    override fun onDestroyView() {
        disposables.clear()
        super.onDestroyView()
    }

    private fun render(state: LoginScreenState) {
        Timber.i("${this.javaClass} is rendering new state: $state")
        when (state) {
            is LoginScreenState.Loading -> showLoading()
            is LoginScreenState.Error -> handleError(state.error)
            is LoginScreenState.Idle -> showLoading(false)
        }
    }

    private fun handleError(error: Throwable) {
        showLoading(false)
        when (error) {
            is LoginFailedException -> toastLong("Login não corresponde a nenhum usuário")
            is LoginEmptyException -> toastLong("Preencha os campos corretamente")
        }
    }

    private fun showLoading(loading: Boolean = true) {
        username_field.isEnabled = !loading
        password_field.isEnabled = !loading
        login_button.isEnabled = !loading
    }
}