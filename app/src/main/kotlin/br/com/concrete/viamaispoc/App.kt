package br.com.concrete.viamaispoc

import android.app.Activity
import android.app.Application
import br.com.concrete.viamaispoc.injection.DaggerAppComponent
import br.com.concrete.viamaispoc.injection.UrlConfig
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject
import timber.log.Timber.DebugTree
import timber.log.Timber



class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        injectComponent()
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    private fun injectComponent() {
        DaggerAppComponent
                .builder()
                .application(this)
                .baseUrl(UrlConfig("https://google.com", "https://media.google.com"))
                .build()
                .inject(this)
    }
}